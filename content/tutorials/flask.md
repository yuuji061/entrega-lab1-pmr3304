---
title: "Espaguete"
date: 2022-10-08T10:30:52-03:00
draft: false
layout: "flask"
---

INGREDIENTES

1. 1 pacote de 500 g de espaguete barilla nº 3
1. 1 bandeja de tomate cereja
1. 1 cabeça de alho inteira
azeite de oliva de qualidade (o suficiente para cobrir o fundo da frigideira)
azeitonas picadas (opcional)
sal e pimenta-do-reino a gosto
1. 1 maço de manjericão fresco
1. 1 fio de óleo
1. 50 g de queijo parmesão ralado

Lave bem os tomates cereja, o manjericão e as azeitonas (se optar por usá-las).
Corte as azeitonas em pequenas rodelas e corte o tomate cereja ao meio; reserve.
Ferva 2 litros de água em uma panela grande.
Adicione à essa água uma colher (sopa) cheia de sal e o fio de óleo para a massa não grudar.
Enquanto a água ferve, leve a cabeça de alho inteira ao micro-ondas por 30 segundos (isso ajuda a descascar os dentes mais facilmente).
DICA
Tome cuidado ao tirar a cabeça de alho do micro-ondas, ela estará quente. Espere alguns segundos e faça um leve corte na casca de cada dente de alho; ao fazer isso, a casca se soltará facilmente, poupando tempo).
Pique o alho e reserve.
Cozinhe o espaguete na água fervente por aproximadamente 5 minutos para ficar al dente (se quiser ele mais cozido, deixe 7 minutos).
Enquanto o espaguete cozinha, aqueça uma frigideira grande.
Coloque o azeite de oliva e o alho por aproximadamente 2 minutos (não deixe o alho dourar pois ele amarga a receita).
Acrescente o tomate cereja e a azeitona e deixa refogar por 2 minutos.
Tempere com sal e pimenta a gosto.
Escorra o macarrão e adicione-o à frigideira.
Acrescente as folhas de manjericão e o queijo ralado.
Mexa por 2 minutos para pegar o gosto e sirva quente.